import izobr_1
import sys
import random
import matplotlib.pyplot as plt
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import (QWidget, QPushButton, QLineEdit,
    QInputDialog, QApplication,QScrollArea, QMainWindow,QFileDialog,  QWidget,QLabel)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QImage,QPixmap
def test_hist_white():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)    
    for i in range(100):
        for j in range(100):
            cl.setRgb(255,255,255,255)
            img.setPixelColor(i,j,cl)
    hist = izobr_1.fhist(img)    
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[255] == 10000
        for el in color_hist[:-1]:
            assert el == 0
            


def test_hist_black():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)    
    for i in range(100):
        for j in range(100):
            cl.setRgb(0,0,0,255)
            img.setPixelColor(i,j,cl)
    hist = izobr_1.fhist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        assert color_hist[0] == 10000
        for el in color_hist[1:]:
            assert el == 0

def test_hist_red():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)    
    for i in range(100):
        for j in range(100):
            cl.setRgb(255,0,0,255)
            img.setPixelColor(i,j,cl)
    hist = izobr_1.fhist(img)
    assert len(hist) == 3
    for color_hist in hist:
        assert len(color_hist) == 256
        assert sum(color_hist) == 10000
        for el in color_hist[1:-1]:
            assert el == 0
    assert hist[0][0] == 0
    assert hist[0][255] == 10000
    assert hist[1][0] == 10000
    assert hist[1][255] == 0
    assert hist[2][0] == 10000
    assert hist[2][255] == 0
def test_copy():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    for i in range(100):
        for j in range(100):
            cl.setRgb(random.randint(0,255),random.randint(0,255),random.randint(0,255),255)
            img.setPixelColor(i,j,cl)
            img1.setPixelColor(i,j,cl)
    img2=izobr_1.fcopy(img1)[0]
    assert img2.width() == img.width()
    assert img2.height() == img.height()
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img.pixelColor(i,j)
def test_offx():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    offx=8
    for i in range(100):
        for j in range(100):           
            x=i+offx
            if x<2*offx:
                cl.setRgb(127,127,127,255)
                img1.setPixelColor(i,j,cl)
            cl.setRgb(random.randint(0,255),random.randint(0,255),random.randint(0,255),255)
            img1.setPixelColor(x,j,cl)
            img.setPixelColor(i,j,cl)
    img2=izobr_1.fdvi(offx,0,img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)
def test_offy():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    offy=9
    for i in range(100):
        for j in range(100):           
            y=j+offy
            if y<offy*2:
                cl.setRgb(127,127,127,255)
                img1.setPixelColor(i,j,cl)
            cl.setRgb(random.randint(0,255),random.randint(0,255),random.randint(0,255),255)
            img1.setPixelColor(i,y,cl)
            img.setPixelColor(i,j,cl)
    img2=izobr_1.fdvi(0,offy,img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)

def test_umnozh_casual_10_40_60():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    k=3
    for i in range(100):
        for j in range(100):           
            cl.setRgb(10*k,40*k,60*k,255)
            img1.setPixelColor(i,j,cl)
            cl.setRgb(10,40,60,255)
            img.setPixelColor(i,j,cl)
    img2=izobr_1.fadj(k,img,1)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)
            
def test_bin_half():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    k=150
    for i in range(100):
        for j in range(100):
            if i<50:
                cl.setRgb(k+1,k+1,k+1,255)
                img1.setPixelColor(i,j,cl)
                img.setPixelColor(i,j,cl)
            else:
                cl.setRgb(k-1,k-1,k-1,255)
                img1.setPixelColor(i,j,cl)
                img.setPixelColor(i,j,cl)
    img2=izobr_1.fbin(k,img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(img2.width()):
        for j in range(img2.height()):
            if i < 50:
                assert img2.pixelColor(i,j).red()==255
                assert img2.pixelColor(i,j).green()==255
                assert img2.pixelColor(i,j).blue()==255
            else:
                assert img2.pixelColor(i,j).red()==0
                assert img2.pixelColor(i,j).green()==0
                assert img2.pixelColor(i,j).blue()==0

def test_raznost_same():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    img0=QImage(100,100,QImage.Format_RGB32)
    for i in range(100):
        for j in range(100):
            cl.setRgb(127,127,127,255)
            img0.setPixelColor(i,j,cl)
            cl.setRgb(random.randint(0,255),random.randint(0,255),random.randint(0,255),255)
            img1.setPixelColor(i,j,cl)
            img.setPixelColor(i,j,cl)
    img2=izobr_1.frazn(img1,img)[0]    
    assert img2.width() == max(img1.width(),img.width())
    assert img2.height() == max(img1.height(),img.height())
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img0.pixelColor(i,j)

def test_raznost_white_black():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    img0=QImage(100,100,QImage.Format_RGB32)
    for i in range(100):
        for j in range(100):
            cl.setRgb(255,255,255,255)
            img0.setPixelColor(i,j,cl)
            cl.setRgb(255,255,255,255)
            img1.setPixelColor(i,j,cl)
            cl.setRgb(127,127,127,255)
            img.setPixelColor(i,j,cl)
    img2=izobr_1.frazn(img1,img)[0]    
    assert img2.width() == max(img1.width(),img.width())
    assert img2.height() == max(img1.height(),img.height())
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img0.pixelColor(i,j)

def test_raznost_purp_blue():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    img0=QImage(100,100,QImage.Format_RGB32)
    for i in range(100):
        for j in range(100):
            
            cl.setRgb(255,127,127,255)
            img0.setPixelColor(i,j,cl)
            
            cl.setRgb(255,0,255,255)
            img1.setPixelColor(i,j,cl)
            
            cl.setRgb(0,0,255,255)
            img.setPixelColor(i,j,cl)
            
    img2=izobr_1.frazn(img1,img)[0]    
    assert img2.width() == max(img1.width(),img.width())
    assert img2.height() == max(img1.height(),img.height())
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img0.pixelColor(i,j)
            assert img2.pixelColor(i,j).red()==255
            assert img2.pixelColor(i,j).green()==127
            assert img2.pixelColor(i,j).blue()==127

def test_sum():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    img0=QImage(100,100,QImage.Format_RGB32)
    for i in range(100):
        for j in range(100):
            r1=random.randint(0,255)
            g1=random.randint(0,255)
            b1=random.randint(0,255)
            r2=random.randint(0,255)
            g2=random.randint(0,255)
            b2=random.randint(0,255)
            rs=r1+r2
            gs=g1+g2
            bs=b1+b2
            if rs>255:
                rs=255
            if gs>255:
                gs=255
            if bs>255:
                bs=255
            cl.setRgb(rs,gs,bs,255)
            img0.setPixelColor(i,j,cl)
            
            cl.setRgb(r1,g1,b1,255)
            img1.setPixelColor(i,j,cl)
            
            cl.setRgb(r2,g2,b2,255)
            img.setPixelColor(i,j,cl)
            
    img2=izobr_1.fsum(img1,img)[0]    
    assert img2.width() == max(img1.width(),img.width())
    assert img2.height() == max(img1.height(),img.height())
    for i in range(img2.width()):
        for j in range(img2.height()):
            assert img2.pixelColor(i,j)==img0.pixelColor(i,j)
def test_lapl_square():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    x1=random.randint(3,55)
    y1=random.randint(3,55)
    x2=random.randint(5,40)
    y2=random.randint(5,40)
    
    for i in range(100):
        for j in range(100):
            
            cl.setRgb(255,255,255,255)
            img.setPixelColor(i,j,cl)
            if i>x1 and i<x1+x2 and j>y1 and j<y1+y2:
                cl.setRgb(0,0,0,255)
                img.setPixelColor(i,j,cl)
    for i in range(100):
        for j in range(100):
            cl.setRgb(127,127,127,255)
            img1.setPixelColor(i,j,cl)
            if i==x1 or i==x1+x2:
                if j>=y1 and j<=y1+y2:
                    cl.setRgb(0,0,0,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1 or j==y1+y2:
                if i>=x1 and i<=x1+x2:
                    cl.setRgb(0,0,0,255)
                    img1.setPixelColor(i,j,cl)
            elif i==x1+1 or i==x1+x2-1:
                if j>=y1+1 and j<=y1+y2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1+1 or j==y1+y2-1:
                if i>=x1+1 and i<=x1+x2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            else:
                cl.setRgb(127,127,127,255)
                img1.setPixelColor(i,j,cl)
    img2=izobr_1.flapl(img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(1,img2.width()-1):
        for j in range(1,img2.height()-1):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)

def test_lprt_square():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    x1=random.randint(3,55)
    y1=random.randint(3,55)
    x2=random.randint(5,40)
    y2=random.randint(5,40)
    
    for i in range(100):
        for j in range(100):
            
            cl.setRgb(255,255,255,255)
            img.setPixelColor(i,j,cl)
            if i>x1 and i<x1+x2 and j>y1 and j<y1+y2:
                cl.setRgb(0,0,0,255)
                img.setPixelColor(i,j,cl)
    for i in range(100):
        for j in range(100):
            cl.setRgb(0,0,0,255)
            img1.setPixelColor(i,j,cl)
            if i==x1 or i==x1+x2:
                if j>=y1 and j<=y1+y2:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1 or j==y1+y2:
                if i>=x1 and i<=x1+x2:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif i==x1+1 or i==x1+x2-1:
                if j>=y1+1 and j<=y1+y2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1+1 or j==y1+y2-1:
                if i>=x1+1 and i<=x1+x2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            else:
                cl.setRgb(0,0,0,255)
                img1.setPixelColor(i,j,cl)
    img2=izobr_1.fprt(img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(1,img2.width()-1):
        for j in range(1,img2.height()-1):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)

def test_lsob_square():
    cl=QColor()
    img=QImage(100,100,QImage.Format_RGB32)
    img1=QImage(100,100,QImage.Format_RGB32)
    x1=random.randint(3,55)
    y1=random.randint(3,55)
    x2=random.randint(5,40)
    y2=random.randint(5,40)
    
    for i in range(100):
        for j in range(100):
            
            cl.setRgb(255,255,255,255)
            img.setPixelColor(i,j,cl)
            if i>x1 and i<x1+x2 and j>y1 and j<y1+y2:
                cl.setRgb(0,0,0,255)
                img.setPixelColor(i,j,cl)
    for i in range(100):
        for j in range(100):
            cl.setRgb(0,0,0,255)
            img1.setPixelColor(i,j,cl)
            if i==x1 or i==x1+x2:
                if j>=y1 and j<=y1+y2:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1 or j==y1+y2:
                if i>=x1 and i<=x1+x2:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif i==x1+1 or i==x1+x2-1:
                if j>=y1+1 and j<=y1+y2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            elif j==y1+1 or j==y1+y2-1:
                if i>=x1+1 and i<=x1+x2-1:
                    cl.setRgb(255,255,255,255)
                    img1.setPixelColor(i,j,cl)
            else:
                cl.setRgb(0,0,0,255)
                img1.setPixelColor(i,j,cl)
    img2=izobr_1.fsob(img)[0]
    assert img2.width() == img1.width()
    assert img2.height() == img1.height()
    for i in range(1,img2.width()-1):
        for j in range(1,img2.height()-1):
            assert img2.pixelColor(i,j)==img1.pixelColor(i,j)
